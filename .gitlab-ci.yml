variables:
  GITLAB_CI_IMAGE_ALPINE: 'alpine:3.16.0'
  GITLAB_CI_IMAGE_DEBIAN: 'debian:bullseye'
  GITLAB_CI_IMAGE_SENTRY_CLI: 'getsentry/sentry-cli'
  GITLAB_CI_IMAGE_DOCKER: 'docker:20.10.18'
  GITLAB_CI_IMAGE_BINFMT: 'tonistiigi/binfmt:qemu-v7.0.0-28'
  GITLAB_CI_DOCKER_BUILDX_PLATFORMS: 'linux/amd64,linux/arm/v7,linux/arm64'
  GITLAB_CI_APT_REPOSITORY: 'deb http://download.opensuse.org/repositories/home:/librespace:/satnogs/Debian_11/ /'
  GITLAB_CI_APT_KEY_URL: 'https://download.opensuse.org/repositories/home:librespace:satnogs/Debian_11/Release.key'
  GITLAB_CI_APT_PACKAGES: >-
    git
    tox
  GITLAB_CI_OBS_PROJECT: 'home:librespace:satnogs'
  GITLAB_CI_SIGN_OFF_EXCLUDE: 'a3fe97202e64477cc59554f2f76299e41a3113f7'
stages:
  - static
  - build
  - test
  - deploy
  - sentry_release
  - security

# 'static' stage
sign_off:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache git
  script: >-
    git log
    --grep "^Signed-off-by: .\+<.\+\(@\| at \).\+\(\.\| dot \).\+>$"
    --invert-grep
    --format="Detected commit '%h' with missing or bad sign-off! Please read 'CONTRIBUTING.md'."
    --exit-code
    $(rev=$(git rev-parse -q --verify "$GITLAB_CI_SIGN_OFF_EXCLUDE^{commit}") && echo "$rev..")

static:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - apt-get -y update
    - apt-get -qy install gnupg libcurl4
    - echo "$GITLAB_CI_APT_REPOSITORY" > /etc/apt/sources.list.d/${GITLAB_CI_OBS_PROJECT}.list
    - apt-key adv --fetch-keys "$GITLAB_CI_APT_KEY_URL"
    - apt-get -q update
    - apt-get -y install $GITLAB_CI_APT_PACKAGES
    - xargs -r -a packages.debian apt-get -y install
  script:
    - tox -e "flake8,isort,yapf,pylint,robot-lint"

# 'build' stage
docs:
  stage: build
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - apt-get -y update
    - apt-get -qy install gnupg libcurl4
    - echo "$GITLAB_CI_APT_REPOSITORY" > /etc/apt/sources.list.d/${GITLAB_CI_OBS_PROJECT}.list
    - apt-key adv --fetch-keys "$GITLAB_CI_APT_KEY_URL"
    - apt-get -q update
    - apt-get -y install $GITLAB_CI_APT_PACKAGES
    - xargs -r -a packages.debian apt-get -y install
  script:
    - rm -rf docs/_build
    - tox -e "docs"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - docs/_build/html
build:
  stage: build
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - apt-get -y update
    - apt-get -qy install gnupg libcurl4
    - echo "$GITLAB_CI_APT_REPOSITORY" > /etc/apt/sources.list.d/${GITLAB_CI_OBS_PROJECT}.list
    - apt-key adv --fetch-keys "$GITLAB_CI_APT_KEY_URL"
    - apt-get -q update
    - apt-get -y install $GITLAB_CI_APT_PACKAGES
    - xargs -r -a packages.debian apt-get -y install
  script:
    - rm -rf dist
    - tox -e "build"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - dist

# 'test' stage
test:
  stage: test
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - apt-get -y update
    - apt-get -qy install gnupg libcurl4
    - echo "$GITLAB_CI_APT_REPOSITORY" > /etc/apt/sources.list.d/${GITLAB_CI_OBS_PROJECT}.list
    - apt-key adv --fetch-keys "$GITLAB_CI_APT_KEY_URL"
    - apt-get -q update
    - apt-get -y install $GITLAB_CI_APT_PACKAGES
    - xargs -r -a packages.debian apt-get -y install
  script:
    - rm -rf robot/output
    - tox -e "pytest,deps,robot"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - robot/output

# 'deploy' stage
deploy:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - apt-get -y update
    - apt-get -qy install gnupg libcurl4
    - echo "$GITLAB_CI_APT_REPOSITORY" > /etc/apt/sources.list.d/${GITLAB_CI_OBS_PROJECT}.list
    - apt-key adv --fetch-keys "$GITLAB_CI_APT_KEY_URL"
    - apt-get -q update
    - apt-get -y install $GITLAB_CI_APT_PACKAGES
    - xargs -r -a packages.debian apt-get -y install
  script:
    - rm -rf dist
    - tox -e "upload"
  only:
    refs:
      - tags
    variables:
      - $PYPI_USERNAME
      - $PYPI_PASSWORD

docker:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_DOCKER}
  variables:
    GNURADIO_IMAGE_TAG: 'satnogs'
  before_script:
    - docker run --privileged --rm ${GITLAB_CI_IMAGE_BINFMT} --uninstall qemu-*
    - docker run --privileged --rm ${GITLAB_CI_IMAGE_BINFMT} --install all
  services:
    - ${GITLAB_CI_IMAGE_DOCKER}-dind
  script:
    - |
      cat << EOF > /etc/buildkitd.toml
      [worker.oci]
      max-parallelism = 1
      EOF
    - >-
      docker buildx
      create
      --config /etc/buildkitd.toml
      --use
      --name container
      --driver docker-container
      --bootstrap
    - 'CACHE_IMAGE="${CI_REGISTRY_IMAGE:+$CI_REGISTRY_IMAGE/satnogs-client:cache}"; export CACHE_IMAGE'
    - '[ -z "$CI_REGISTRY_IMAGE" ] || docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY'
    - '[ -z "$DOCKERHUB_PASSWORD" ] || docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASSWORD'
    - >-
      docker buildx
      bake
      --progress plain
      -f docker-compose.yml
      ${CI_REGISTRY_IMAGE:+ -f docker-compose.cache.yml}
      --pull
      --set "*.platform=${GITLAB_CI_DOCKER_BUILDX_PLATFORMS}"
      ${CACHE_IMAGE:+ --set "*.cache-to=type=registry,ref=$CACHE_IMAGE"}
      ${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/satnogs-client:${CI_COMMIT_REF_NAME}"}
      ${CI_COMMIT_TAG:+${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/satnogs-client:latest"}}
      ${CI_REGISTRY_IMAGE:+ --push}
      ${DOCKERHUB_PASSWORD:+ --set "*.tags=librespace/satnogs-client:${CI_COMMIT_REF_NAME}"}
      ${CI_COMMIT_TAG:+${DOCKERHUB_PASSWORD:+ --set "*.tags=librespace/satnogs-client:latest"}}
      ${DOCKERHUB_PASSWORD:+ --push}
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

docker_unstable:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_DOCKER}
  variables:
    GNURADIO_IMAGE_TAG: 'satnogs-unstable'
  before_script:
    - docker run --privileged --rm ${GITLAB_CI_IMAGE_BINFMT} --uninstall qemu-*
    - docker run --privileged --rm ${GITLAB_CI_IMAGE_BINFMT} --install all
  services:
    - ${GITLAB_CI_IMAGE_DOCKER}-dind
  script:
    - |
      cat << EOF > /etc/buildkitd.toml
      [worker.oci]
      max-parallelism = 1
      EOF
    - >-
      docker buildx
      create
      --config /etc/buildkitd.toml
      --use
      --name container
      --driver docker-container
      --bootstrap
    - 'CACHE_IMAGE="${CI_REGISTRY_IMAGE:+$CI_REGISTRY_IMAGE/satnogs-client:cache-unstable}"; export CACHE_IMAGE'
    - '[ -z "$CI_REGISTRY_IMAGE" ] || docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY'
    - '[ -z "$DOCKERHUB_PASSWORD" ] || docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASSWORD'
    - >-
      docker buildx
      bake
      --progress plain
      -f docker-compose.yml
      ${CI_REGISTRY_IMAGE:+ -f docker-compose.cache.yml}
      --pull
      --set "*.platform=${GITLAB_CI_DOCKER_BUILDX_PLATFORMS}"
      ${CACHE_IMAGE:+ --set "*.cache-to=type=registry,ref=$CACHE_IMAGE"}
      ${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/satnogs-client:${CI_COMMIT_REF_NAME}-unstable"}
      ${CI_COMMIT_TAG:+${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/satnogs-client:unstable"}}
      ${CI_REGISTRY_IMAGE:+ --push}
      ${DOCKERHUB_PASSWORD:+ --set "*.tags=librespace/satnogs-client:${CI_COMMIT_REF_NAME}-unstable"}
      ${CI_COMMIT_TAG:+${DOCKERHUB_PASSWORD:+ --set "*.tags=librespace/satnogs-client:unstable"}}
      ${DOCKERHUB_PASSWORD:+ --push}
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pages:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_ALPINE}
  script:
    - mv docs/_build/html/ public/
  artifacts:
    paths:
      - public
  only:
    - tags

# 'sentry_release' stage
sentry_release:
  stage: sentry_release
  image: ${GITLAB_CI_IMAGE_SENTRY_CLI}
  script:
    - sentry-cli releases new --finalize -p ${CI_PROJECT_NAME} ${CI_PROJECT_NAME}@${CI_COMMIT_TAG}
    - sentry-cli releases set-commits --auto ${CI_PROJECT_NAME}@${CI_COMMIT_TAG}
  only:
    refs:
      - tags
    variables:
      - $SENTRY_AUTH_TOKEN
      - $SENTRY_ORG

# 'security' stage
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
dependency_scanning:
  stage: security
  needs: []
  variables:
    DS_EXCLUDED_ANALYZERS: 'gemnasium-maven'
sast:
  stage: security
  needs: []
secret_detection:
  stage: security
  needs: []
license_scanning:
  stage: security
  needs: []
container_scanning:
  stage: security
  needs:
    - job: docker
      artifacts: false
  variables:
    CI_APPLICATION_REPOSITORY: ${CI_REGISTRY_IMAGE}/satnogs-client
    CI_APPLICATION_TAG: ${CI_COMMIT_REF_NAME}
  rules:
    - if: $CI_REGISTRY_IMAGE && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
